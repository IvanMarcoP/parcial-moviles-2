using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield_Controller : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Player_Controller pc = other.GetComponent<Player_Controller>();

            if (pc != null)
            {
                pc.ActivateShield(true);
                pc.escudosRecolectados++;
                GestorDeAudio.instancia.ReproducirSonido("Escudo");
            }

            gameObject.SetActive(false);
        }
        else if (other.CompareTag("Wall"))
        {
            gameObject.SetActive(false);
        }
    }
}
