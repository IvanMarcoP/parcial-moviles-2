using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public class ControladorDatosJuego : MonoBehaviour
{
    public GameObject jugador;
    public GameManager gameManager;
    public string archivoDeGuardado;
    public DatosJuego datosJuego = new DatosJuego();

    // Update is called once per frame
    void Awake()
    {
        archivoDeGuardado = Application.persistentDataPath + "/datosJuego.json";

        jugador = GameObject.FindGameObjectWithTag("Player");
    }


    public void CargarDatos()
    {
        if (File.Exists(archivoDeGuardado))
        {
            string contenido = File.ReadAllText(archivoDeGuardado);
            datosJuego = JsonUtility.FromJson<DatosJuego>(contenido);

            jugador.GetComponent<Player_Controller>().vidasPerdidas = datosJuego.vidasPerdidas;
            jugador.GetComponent<Player_Controller>().score = datosJuego.monedas;
            jugador.GetComponent<Player_Controller>().escudosRecolectados = datosJuego.escudosRecolectados;
            //gameManager.puntuacionActual = datosJuego.puntuacionActual;
            gameManager.highScore = datosJuego.highScore;
        }
        else
        {
            Debug.Log("No se han encontrado archivos Guardados");
        }
    }

    public void GuardarDatos()
    {
        DatosJuego nuevosDatos = new DatosJuego()
        {
            vidasPerdidas = jugador.GetComponent<Player_Controller>().vidasPerdidas,
            monedas = jugador.GetComponent<Player_Controller>().score,
            //puntuacionActual = gameManager.puntuacionActual,
            escudosRecolectados = jugador.GetComponent<Player_Controller>().escudosRecolectados,
            highScore = gameManager.highScore
        };

        string cadenaJson = JsonUtility.ToJson(nuevosDatos);

        File.WriteAllText(archivoDeGuardado, cadenaJson);
    }
}
