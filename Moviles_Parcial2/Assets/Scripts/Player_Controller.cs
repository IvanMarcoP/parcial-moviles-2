using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Threading;
using System;

public class Player_Controller : MonoBehaviour
{
    private Rigidbody2D rb;

    [Header("Datos Pj")]
    public float speed = 10f;
    public float jumpForceMultiplier = 10f;
    public float maxJumpChargeTime = 2f;
    private bool isChargingJump = false;
    private float jumpChargeTime = 0f;

    [Header("Datos piso")]
    public Transform groundCheck;
    public LayerMask groundLayer;
    public float checkRadius = 0.1f;

    [Header("Datos UI")]
    public Slider jumpChargeSlider;
    public Vector2 sliderOffset = new Vector2(50, 50);
    public TMP_Text textScore;
    public int score = 0;

    [Header("Shield & Life")]
    public int currentLife;
    public int vidasPerdidas;
    [SerializeField] private int maxLifes = 3;
    public GameObject shield;
    public int escudosRecolectados;
    public bool isActiveShield;

    [Header("Plataforma Jugador")]
    public GameObject plataformaBackUp;
    public Transform posicionPlataforma;
    public float tiempoPlataforma = 3f;
    public bool plataformaActive;
    public GameObject iconoPlataformaBackUp;

    [Header("IFrames")]
    public float invulnerabilityDuration = 2f;
    private bool isInvulnerable = false;
    private SpriteRenderer spriteRen;

    [Header("Scripts")]
    public HealthUI healthUI;
    public GameManager GM;
    public GameManagerNiveles GMN;
    private bool isGrounded;
    private bool isIncreasing;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        shield.SetActive(false);
        currentLife = maxLifes;
        jumpChargeSlider.maxValue = maxJumpChargeTime;
        jumpChargeSlider.value = 0f;
        jumpChargeSlider.gameObject.SetActive(false);
        spriteRen = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckGroundStatus();
        CargarSalto();
        //CargarSaltoMouse();
        InstanciarPlataformaTouch();
        InstanciarPlataformaTeclado();

        UpdateText();
        UpdateIcon();
        if (isGrounded == false)
        {
            MovimientoLateral();
            //MovimientoLateralTeclado();
        }

        UpdateSliderPosition();
    }

    void MovimientoLateral()
    {
        Vector2 tilt = Input.acceleration;

        Vector2 move = new Vector2(tilt.x, 0) * speed;

        rb.velocity = new Vector2(move.x, rb.velocity.y);
    }

    void CargarSalto()
    {
        if (Input.touchCount > 0 && isGrounded)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                isChargingJump = true;
                jumpChargeTime = 0f;
                jumpChargeSlider.gameObject.SetActive(true);
                isIncreasing = true;
            }

            if (touch.phase == TouchPhase.Stationary && isChargingJump)
            {
                if (isIncreasing)
                {
                    jumpChargeTime += Time.deltaTime;
                    if (jumpChargeTime >= maxJumpChargeTime)
                    {
                        jumpChargeTime = maxJumpChargeTime;
                        isIncreasing = false;
                    }
                }
                else
                {
                    jumpChargeTime -= Time.deltaTime;
                    if (jumpChargeTime <= 0f)
                    {
                        jumpChargeTime = 0f;
                        isIncreasing = true;
                    }
                }

                jumpChargeTime = Mathf.Clamp(jumpChargeTime, 0f, maxJumpChargeTime);
                jumpChargeSlider.value = jumpChargeTime;
            }

            if (touch.phase == TouchPhase.Ended && isChargingJump)
            {
                RealizarSalto();
                isChargingJump = false;
                jumpChargeSlider.gameObject.SetActive(false);
            }
        }
    }

    void InstanciarPlataformaTouch()
    {
        if (!isGrounded && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && plataformaActive)
        {
            GameObject backupPlatform = Instantiate(plataformaBackUp, posicionPlataforma.position, Quaternion.identity);
            plataformaActive = false;
            Destroy(backupPlatform, tiempoPlataforma);
        }
    }

    void MovimientoLateralTeclado()
    {
        float moveInput = Input.GetAxis("Horizontal");
        Vector2 move = new Vector2(moveInput * speed, rb.velocity.y);
        rb.velocity = move;
    }

    void CargarSaltoMouse()
    {
        if (Input.GetMouseButtonDown(0) && isGrounded)
        {
            isChargingJump = true;
            jumpChargeTime = 0f;
            jumpChargeSlider.gameObject.SetActive(true);
            isIncreasing = true;
        }

        if (Input.GetMouseButton(0) && isChargingJump)
        {
            if (isIncreasing)
            {
                jumpChargeTime += Time.deltaTime;
                if (jumpChargeTime >= maxJumpChargeTime)
                {
                    jumpChargeTime = maxJumpChargeTime;
                    isIncreasing = false;
                }
            }
            else
            {
                jumpChargeTime -= Time.deltaTime;
                if (jumpChargeTime <= 0f)
                {
                    jumpChargeTime = 0f;
                    isIncreasing = true;
                }
            }

            jumpChargeTime = Mathf.Clamp(jumpChargeTime, 0f, maxJumpChargeTime);
            jumpChargeSlider.value = jumpChargeTime;
        }

        if (Input.GetMouseButtonUp(0) && isChargingJump)
        {
            RealizarSalto();
            isChargingJump = false;
            jumpChargeSlider.gameObject.SetActive(false);
        }
    }

    void InstanciarPlataformaTeclado()
    {
        if(!isGrounded && Input.GetMouseButtonDown(0) && plataformaActive)
        {
            GameObject backupPlatform = Instantiate(plataformaBackUp, posicionPlataforma.position, Quaternion.identity);
            plataformaActive = false;
            Destroy(backupPlatform, tiempoPlataforma);
        }
    }

    void RealizarSalto()
    {
        float jumpForce = jumpChargeTime * jumpForceMultiplier;
        rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
    }

    void CheckGroundStatus()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, groundLayer);
    }

    void UpdateSliderPosition()
    {
        // Convertir la posici�n del jugador a la posici�n de pantalla
        Vector2 playerScreenPosition = Camera.main.WorldToScreenPoint(transform.position);

        // Aplicar el offset y ajustar la posici�n del Slider
        jumpChargeSlider.transform.position = playerScreenPosition + sliderOffset;
    }

    public void AddScore(int value)
    {
        score += value;
        UpdateText();
    }

    public void ApplyDamage(int damage)
    {
        if (isInvulnerable)
        {
            return;
        }


        if (isActiveShield)
        {
            BreakShield();
        }
        else
        {
            currentLife -= damage;
            vidasPerdidas++;

            if (currentLife <= 0)
            {
                GM.SetGameOver();
                GMN.SetGameOver();
                Debug.Log("Muerto");
            }

            StartCoroutine(InvulnerabilityCoroutine());
        }

        healthUI.UpdateHearts(currentLife, maxLifes);
    }

    private IEnumerator InvulnerabilityCoroutine()
    {
        isInvulnerable = true;
        float elapsedTime = 0f;

        while (elapsedTime < invulnerabilityDuration)
        {
            spriteRen.enabled = !spriteRen.enabled; // Parpadeo
            yield return new WaitForSeconds(0.1f);
            elapsedTime += 0.1f;
        }

        spriteRen.enabled = true; // Asegurarse de que el sprite est� visible al final
        isInvulnerable = false;
    }


    public void HealPLayer(int amount)
    {
        currentLife += amount;

        if (currentLife + amount > maxLifes)
        {
            currentLife = maxLifes;
        }

        healthUI.UpdateHearts(currentLife, maxLifes);
    }

    public void ActivateShield(bool state)
    {
        isActiveShield = state;
        if (shield != null)
        {
            shield.SetActive(state);
        }
    }

    private void BreakShield()
    {
        isActiveShield = false;
        shield.SetActive(false);
    }

    void UpdateText()
    {
        if (textScore != null)
        {
            textScore.text = score.ToString();
        }
    }

    void UpdateIcon()
    {
        if(iconoPlataformaBackUp != null)
        {
            iconoPlataformaBackUp.SetActive(plataformaActive);
        }
    }
}

//private IEnumerator Invulnerability()
//{
//    Physics2D.IgnoreLayerCollision(10, 11, true);

//    for (int i = 0; i < numberFlashes; i++)
//    {
//        spriteRen.color = new Color(1, 0, 0, 0.5f);
//        yield return new WaitForSeconds(iFramesDuracion / (numberFlashes * 2));
//        spriteRen.color = Color.white;
//        yield return new WaitForSeconds(iFramesDuracion / (numberFlashes * 2));
//    }

//    Physics2D.IgnoreLayerCollision(10, 11, false);
//}