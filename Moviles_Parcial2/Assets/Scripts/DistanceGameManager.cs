using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DistanceGameManager : MonoBehaviour
{
    [Header("Points")]
    public Transform player;
    public TMP_Text distanceText;
    public TMP_Text highScoreText;
    private int currentHeight;
    private int highPoint;
    public int highScore;

    // Start is called before the first frame update
    void Start()
    {
        LoadHighScore();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateDistance();
    }

    void UpdateDistance()
    {
        int current = Mathf.FloorToInt(player.position.y);

        if (current > highPoint)
        {
            highPoint = current;
            distanceText.text = highPoint.ToString() + " m";

            if (highPoint > highScore)
            {
                highScore = highPoint;
                SaveHighScore();
            }
        }
    }

    void SaveHighScore()
    {
        PlayerPrefs.SetInt("HighScore", highScore);
        PlayerPrefs.Save();
    }

    void LoadHighScore()
    {
        highScore = PlayerPrefs.GetInt("HighScore", 0);
        highScoreText.text = "HighScore: " + highScore.ToString() + " m";
    }
}
