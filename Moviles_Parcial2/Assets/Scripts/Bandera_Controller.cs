using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bandera_Controller : MonoBehaviour
{
    public GameObject WinScreen;

    void Start()
    {
        WinScreen.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            MostrarMenuVictoria();
        }
    }

    void MostrarMenuVictoria()
    {
        WinScreen.SetActive(true);
    }
}
