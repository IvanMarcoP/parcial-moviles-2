using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma_Trampolin : MonoBehaviour
{
    public float springForce = 10f;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Rigidbody2D rb = other.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                rb.velocity = new Vector2(rb.velocity.x, 0f);
                rb.AddForce(Vector2.up * springForce, ForceMode2D.Impulse);
            }
        }
    }
}
