using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParedLimite : MonoBehaviour
{
    public Transform player; // Referencia al transform del jugador
    public float offsetY = 5f; // Distancia vertical entre el jugador y la pared

    void Update()
    {
        if (player != null)
        {
            // Obtener la posici�n actual de la pared
            Vector3 currentPos = transform.position;

            // Seguir verticalmente al jugador manteniendo la posici�n horizontal
            currentPos.y = player.position.y + offsetY;

            // Asignar la nueva posici�n a la pared
            transform.position = currentPos;
        }
    }
}
