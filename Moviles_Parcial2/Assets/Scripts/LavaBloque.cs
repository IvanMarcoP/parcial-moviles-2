using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaBloque : MonoBehaviour
{
    public float speed = 2f; // Velocidad a la que se mueve el bloque rojo
    private GameManager gameManager;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        // Mover el bloque rojo hacia arriba continuamente
        transform.Translate(Vector2.up * speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            // Si colisiona con el jugador, establecer el estado de Game Over
            gameManager.SetGameOver();
        }
    }
}
