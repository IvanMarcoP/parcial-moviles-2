using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraMovimiento_Controller : MonoBehaviour
{
    public enum MovementType
    {
        Horizontal,
        Vertical
    }

    public MovementType movementType = MovementType.Horizontal;

    public float speed = 2f;
    public float amplitude = 3f;
    private float initialX;
    private float initialY;
    private float startTime;

    public int damage = 1;

    private void OnEnable()
    {
        initialX = transform.position.x;
        initialY = transform.position.y;
        startTime = Time.time;
    }

    private void Update()
    {
        MovePlatform();
    }

    private void MovePlatform()
    {
        float elapsedTime = Time.time - startTime;

        if (movementType == MovementType.Horizontal)
        {
            float newX = initialX + Mathf.Sin(elapsedTime * speed) * amplitude;
            transform.position = new Vector3(newX, initialY, transform.position.z);
        }
        else if (movementType == MovementType.Vertical)
        {
            float newY = initialY + Mathf.Sin(elapsedTime * speed) * amplitude;
            transform.position = new Vector3(initialX, newY, transform.position.z);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Player_Controller pc = other.GetComponent<Player_Controller>();
            if (pc != null)
            {
                pc.ApplyDamage(damage);
                GestorDeAudio.instancia.ReproducirSonido("Da�o");
            }
        }
        else if (other.CompareTag("Wall"))
        {
            gameObject.SetActive(false);
        }
    }
}
