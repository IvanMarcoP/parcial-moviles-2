using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DatosJuego
{
    public int vidasPerdidas;
    public int monedas;
    public int escudosRecolectados;
    //public int puntuacionActual;
    public int highScore;
}
