using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesaparecerObjetos : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Wall"))
        {
            gameObject.SetActive(false);
        }
    }
}
