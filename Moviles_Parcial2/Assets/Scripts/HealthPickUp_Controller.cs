using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUp_Controller : MonoBehaviour
{
    private int hpIncrease = 1;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Player_Controller pc = other.GetComponent<Player_Controller>();

            if (pc != null)
            {
                pc.HealPLayer(hpIncrease);
                GestorDeAudio.instancia.ReproducirSonido("Curacion");

            }

            gameObject.SetActive(false);
        }
        else if (other.CompareTag("Wall"))
        {
            gameObject.SetActive(false);
        }
    }
}
