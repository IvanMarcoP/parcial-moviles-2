using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Security.Cryptography;
using static System.Net.Mime.MediaTypeNames;

public class GameManager : MonoBehaviour
{
    [Header("Object Pool")]
    public ObjectPool objectPool;
    public ControladorDatosJuego controlDatosJuego;

    [Header("Points")]
    public Transform player;
    public TMP_Text distanceText;
    public TMP_Text highScoreText;
    public TMP_Text currentScoreText;
    public int puntuacionActual;
    public int highScore;


    [Header("Prefabs")]
    public GameObject coinPrefab;
    public GameObject hpPickUp;
    public GameObject plataformaNormal;
    public GameObject plataformaFragil;
    public GameObject plataformaTrampolin;
    public GameObject barraHorizontal;
    public GameObject barraVertical;
    public GameObject escudoPrefab;
    public GameObject backUpItem;

    [Header("UI Elements")]
    public GameObject gameOverScreen;
    public bool gameOver = false;

    //// Start is called before the first frame update
    void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("CancionInfinito");
        GestorDeAudio.instancia.PausarSonido("CancionMenu");
        gameOver = false;
        gameOverScreen.gameObject.SetActive(false);
        ResumeGame();
        InicializarPools();
        controlDatosJuego.CargarDatos();
        LoadHighScore();
    }

    void InicializarPools()
    {
        objectPool.InitializePool(coinPrefab, 20);
        objectPool.InitializePool(hpPickUp, 2);
        objectPool.InitializePool(plataformaNormal, 10);
        objectPool.InitializePool(plataformaFragil, 10);
        objectPool.InitializePool(plataformaTrampolin, 10);
        objectPool.InitializePool(barraHorizontal, 1);
        objectPool.InitializePool(barraVertical, 1);
        objectPool.InitializePool(escudoPrefab, 1);
        objectPool.InitializePool(backUpItem, 2);
    }

    void Update()
    {
        UpdateDistance();

        if (gameOver)
        {
            gameOverScreen.gameObject.SetActive(true);
            controlDatosJuego.GuardarDatos();
            controlDatosJuego.CargarDatos();
            Time.timeScale = 0f;
            highScoreText.text = "HighScore: " + controlDatosJuego.datosJuego.highScore.ToString() + " m";
            currentScoreText.text = "Score: " + puntuacionActual.ToString() + " m";
        }

    }

    void UpdateDistance()
    {
        int current = Mathf.FloorToInt(player.position.y);

        if(current > puntuacionActual)
        {
            puntuacionActual = current;
            distanceText.text = puntuacionActual.ToString() + " m";

            if(puntuacionActual > highScore)
            {
                highScore = puntuacionActual;
                SaveHighScore();
            }
        }
    }

    void SaveHighScore()
    {
        PlayerPrefs.SetInt("HighScore",  highScore);
        PlayerPrefs.Save();
    }

    void LoadHighScore()
    {
        highScore = PlayerPrefs.GetInt("HighScore", 0);
        highScoreText.text = "HighScore: " + highScore.ToString() + " m";
    }

    public GameObject SpawnObject(GameObject prefab, Vector2 position)
    {
        GameObject obj = objectPool.GetPooledObject(prefab);
        if (obj != null)
        {
            obj.transform.position = position;
            obj.SetActive(true);
        }
        return obj;
    }


    public void SetGameOver()
    {
        gameOver = true;
    }

    public void RestartScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
        ResumeGame();
        GestorDeAudio.instancia.ReproducirSonido("CancionInfinito");
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("Menu_Inicio");
        GestorDeAudio.instancia.PausarSonido("CancionInfinito");
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1f;
    }
}
