using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicaFondo : MonoBehaviour
{

    public string cancion;

    // Start is called before the first frame update
    void Start()
    {
        ReproducirMusica(cancion);
    }

    void ReproducirMusica(string music)
    {
        GestorDeAudio.instancia.ReproducirSonido(music);
    }

    void DetenerMusica()
    {
        GestorDeAudio.instancia.PausarSonido(cancion);
    }
}
