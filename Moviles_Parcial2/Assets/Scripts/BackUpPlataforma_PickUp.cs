using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackUpPlataforma_PickUp : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Player_Controller player = other.GetComponent<Player_Controller>();
            if (player != null)
            {
                player.plataformaActive = true;
                GestorDeAudio.instancia.ReproducirSonido("BloquePiso");
                gameObject.SetActive(false);
            }
        }
    }
}
