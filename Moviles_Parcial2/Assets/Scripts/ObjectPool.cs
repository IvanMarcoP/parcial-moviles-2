using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    private Dictionary<string, List<GameObject>> pools = new Dictionary<string, List<GameObject>>();

    public void InitializePool(GameObject prefab, int poolSize)
    {
        string key = prefab.name;
        if (!pools.ContainsKey(key))
        {
            List<GameObject> objectPool = new List<GameObject>();
            for (int i = 0; i < poolSize; i++)
            {
                GameObject obj = Instantiate(prefab, transform);
                obj.SetActive(false);
                objectPool.Add(obj);
            }
            pools.Add(key, objectPool);
        }
    }

    public GameObject GetPooledObject(GameObject prefab)
    {
        string key = prefab.name;
        if (pools.ContainsKey(key))
        {
            foreach (GameObject obj in pools[key])
            {
                if (!obj.activeInHierarchy)
                {
                    return obj;
                }
            }

            // Si no hay objetos inactivos, agregar uno nuevo al pool
            //GameObject newObj = Instantiate(prefab, transform);
            //newObj.SetActive(false);
            //pools[key].Add(newObj);
            //return newObj;
        }
        else
        {
            // Si el tipo de objeto no tiene un pool, devolver null
            Debug.LogWarning("No se ha inicializado el pool para el objeto: " + key);
            return null;
        }

        return null;
    }
}
