using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class GameManagerNiveles : MonoBehaviour
{
    [Header("UI Elements")]
    public GameObject gameOverScreen;
    public bool gameOver = false;

    //// Start is called before the first frame update
    void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("CancionNiveles");
        GestorDeAudio.instancia.PausarSonido("CancionMenu");
        gameOver = false;
        gameOverScreen.gameObject.SetActive(false);
        ResumeGame();
    }

    void Update()
    {
        if (gameOver)
        {
            gameOverScreen.gameObject.SetActive(true);
            Time.timeScale = 0f;
        }

    }


    public void SetGameOver()
    {
        gameOver = true;
    }

    public void RestartScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
        ResumeGame();
        GestorDeAudio.instancia.ReproducirSonido("CancionNiveles");
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("Menu_Inicio");
        GestorDeAudio.instancia.PausarSonido("CancionNiveles");
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1f;
    }
}
