using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraSpawner : MonoBehaviour
{
    public GameManager gameManager;
    public Transform playerTransform;

    public float spawnDistance = 3f; // Distancia por delante del jugador donde se generan los objetos
    public float minX = -2f;          // Mínima posición X para la generación de objetos
    public float maxX = 2f;
    public float spawnInterval = 2f;  // Intervalo de generación en unidades
    public float yOffset = 1f;
    private float nextSpawnY;
    private int currentPrefabIndex = 0;

    private List<GameObject> prefabs;

    private void Start()
    {
        nextSpawnY = playerTransform.position.y + spawnInterval;

        prefabs = new List<GameObject>
        {
            gameManager.barraHorizontal,
            gameManager.barraVertical,
        };
    }

    private void Update()
    {
        if (playerTransform.position.y + spawnDistance > nextSpawnY)
        {
            SpawnNextObject();
            nextSpawnY += spawnInterval + yOffset; // Incrementar la distancia de spawn con el offset
        }
    }

    private void SpawnNextObject()
    {
        GameObject prefab = prefabs[currentPrefabIndex];
        Vector2 spawnPosition = new Vector2(
            Random.Range(minX, maxX),
            nextSpawnY
        );

        GameObject obj = gameManager.SpawnObject(prefab, spawnPosition);
        if (obj != null)
        {
            obj.SetActive(true);
        }

        currentPrefabIndex = (currentPrefabIndex + 1) % prefabs.Count;
    }
}
