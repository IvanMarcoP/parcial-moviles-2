using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{

    void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("CancionMenu");
    }

    public void PlayGame(string levelName)
    {
        // Cargar la escena del juego. Cambia "GameScene" por el nombre de la escena que quieres cargar.
        SceneManager.LoadScene(levelName);
        GestorDeAudio.instancia.PausarSonido("CancionMenu");
    }

    public void ExitGame()
    {
        // Sale de la aplicación
        Application.Quit();
    }

}
