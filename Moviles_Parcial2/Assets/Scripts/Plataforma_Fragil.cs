using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma_Fragil : MonoBehaviour
{
    public int maxJumpsBeforeBreak = 3;
    public float breakDelay = 2f;
    public Color normalColor = Color.blue;
    private int jumpsCount = 0;
    private bool isBreaking = false;
    private SpriteRenderer spriteRenderer;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.color = normalColor; // Al inicio, establecer el color normal
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            jumpsCount++;

            // Cambiar color gradualmente basado en el tiempo restante antes de romperse
            float percentRemaining = 1f - (float)jumpsCount / maxJumpsBeforeBreak;
            Color platformColor = Color.Lerp(Color.red, Color.blue, percentRemaining);
            GetComponent<SpriteRenderer>().color = platformColor;

            // Verificar si alcanz� el m�ximo de saltos
            if (jumpsCount >= maxJumpsBeforeBreak && !isBreaking)
            {
                isBreaking = true;
                StartCoroutine(BreakPlatform());
            }
        }
    }

    IEnumerator BreakPlatform()
    {
        yield return new WaitForSeconds(breakDelay);
        jumpsCount = 0;
        isBreaking = false;
        spriteRenderer.color = normalColor; // Restaurar color normal
        gameObject.SetActive(false);
    }
}
