using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    public GameManager gameManager;
    public Transform playerTransform;

    public float spawnDistance = 10f; // Distancia por delante del jugador donde se generan los objetos
    public float minX = -2f;          // M�nima posici�n X para la generaci�n de objetos
    public float maxX = 2f;
    public float minSpawnY = 1f;      // M�nima altura para la generaci�n de objetos
    public float maxSpawnY = 3f;      // M�xima altura para la generaci�n de objetos
    public float spawnInterval = 5f;  // Intervalo de generaci�n en unidades

    private float nextSpawnY;
    private int currentPrefabIndex = 0;

    private List<GameObject> prefabs;

    private void Start()
    {
        nextSpawnY = playerTransform.position.y + spawnInterval;

        prefabs = new List<GameObject>
        {
            gameManager.coinPrefab,
        };
    }

    private void Update()
    {
        if (playerTransform.position.y + spawnDistance > nextSpawnY)
        {
            SpawnNextObject();
            nextSpawnY += spawnInterval;
        }
    }

    private void SpawnNextObject()
    {
        GameObject prefab = prefabs[currentPrefabIndex];
        Vector2 spawnPosition = new Vector2(
            Random.Range(minX, maxX),
            nextSpawnY + Random.Range(minSpawnY, maxSpawnY)
        );

        GameObject obj = gameManager.SpawnObject(prefab, spawnPosition);
        if (obj != null)
        {
            obj.SetActive(true);
        }

        currentPrefabIndex = (currentPrefabIndex + 1) % prefabs.Count;
    }
}
